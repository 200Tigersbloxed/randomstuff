using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GCKCounter
{
    public static class CounterManager
    {
        public static bool updateTimer = false;
        public static float time = 0.00f;

        public static void StartTimer()
        {
            time = 0.00f;
            updateTimer = true;
        }
        public static void StopTimer() => updateTimer = false;

        public static void AttachCounterStop(Scene currentScene)
        {
            if(currentScene.name == "Bank Crash")
            {
                GameObject[] rootObjects = currentScene.GetRootGameObjects();
                GameObject Scene2 = null;
                foreach(GameObject child in rootObjects)
                {
                    if (child.name == "Scene2") Scene2 = child;
                }
                Scene2.AddComponent<BankEndScene>();
            }
            else
            {
                MelonLoader.MelonLogger.Warning("Cannot add BankEndScene if not in Bank Crash scene!");
            }
        }
    }

    public class BankEndScene : MonoBehaviour
    {
        bool loopEnabled = true;
        // attach this to the disabled game object
        void Update()
        {
            if (loopEnabled)
            {
                if (this.gameObject.activeSelf)
                {
                    loopEnabled = false;
                    CounterManager.StopTimer();
                }
            }
        }
    }
}
