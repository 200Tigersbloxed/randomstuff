using System.Runtime.InteropServices;
using GCKCounter;
using MelonLoader;

// In SDK-style projects such as this one, several assembly attributes that were historically
// defined in this file are now automatically added during build and populated with
// values defined in project properties. For details of which attributes are included
// and how to customise this process see: https://aka.ms/assembly-info-properties


// Setting ComVisible to false makes the types in this assembly not visible to COM
// components.  If you need to access a type in this assembly from COM, set the ComVisible
// attribute to true on that type.

[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM.

[assembly: Guid("f28b4d8f-ea9a-41a3-8771-9246df41f2a0")]
[assembly: MelonInfo(typeof(MainMod), "GCKCounter", "v1.1.0", "200Tigersbloxed")]
[assembly: MelonGame("Yurtle Bezbro Virtual", "Grandpas Car Keys")]
