using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace GCKCounter
{
    public static class UIManager
    {
        public static Text currentText = null;

        public static GameObject FindCanvas() { return GameObject.Find("GCKCounterCanvas"); }

        public static Text CreateText()
        {
            // Create the GameObject and Add the Canvas
            GameObject myGO = new GameObject
            {
                name = "GCKCounterCanvas"
            };
            myGO.transform.SetAsFirstSibling();
            myGO.AddComponent<Canvas>();
            // Get the Canvas Component and set its RenderMode
            Canvas myCanvas = myGO.GetComponent<Canvas>();
            myCanvas.renderMode = RenderMode.ScreenSpaceOverlay;
            // Add Canvas Stuff idk it's just in the unity docs
            myGO.AddComponent<CanvasScaler>();
            myGO.AddComponent<GraphicRaycaster>();
            // Add the Text GameObject
            GameObject myTextGO = new GameObject();
            myTextGO.transform.parent = myGO.transform;
            myTextGO.name = "GCKCounterText";
            // Add the Text Component
            Text myText = myTextGO.AddComponent<Text>();
            myText.text = "Counter";
            myText.font = Resources.GetBuiltinResource(typeof(Font), "Arial.ttf") as Font;
            myText.fontSize = 50;
            myText.alignment = TextAnchor.UpperRight;
            // Text Position
            RectTransform textRectTransform = myText.GetComponent<RectTransform>();
            textRectTransform.localPosition = new Vector3(500, 200, 0);
            textRectTransform.sizeDelta = new Vector2(400, 200);
            // Update global value
            currentText = myText;
            // Return our new Text
            return myText;
        }

        public static class StartButtonManager
        {
            public static void AttachToMenuButton(Scene menuScene)
            {
                // Find the button
                GameObject[] objects = menuScene.GetRootGameObjects();
                GameObject targetParent = null;
                foreach(GameObject child in objects)
                {
                    if (child.name == "Canvas") targetParent = child;
                }
                // find the button
                Button targetButton = null;
                targetButton = targetParent.transform.GetChild(0).gameObject.GetComponent<Button>();
                // attach to the onclick event
                targetButton.onClick.RemoveAllListeners();
                targetButton.onClick.AddListener(CounterManager.StartTimer);
            }
        }
    }
}
