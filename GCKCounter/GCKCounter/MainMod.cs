using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MelonLoader;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GCKCounter
{
    public class MainMod : MelonMod
    {
        bool enableUpdate = false;
        bool isCounting = false;
        Scene thisScene;

        public override void OnSceneWasLoaded(int buildIndex, string sceneName)
        {
            Scene currentScene = SceneManager.GetSceneByName(sceneName);
            thisScene = currentScene;
            UIManager.CreateText();
            switch (sceneName)
            {
                case "Menu":
                    // Add button listener
                    UIManager.StartButtonManager.AttachToMenuButton(currentScene);
                    break;
                case "Bank Crash":
                    // Setup special monobehaviour
                    CounterManager.AttachCounterStop(currentScene);
                    break;
            }
            enableUpdate = true;
            base.OnSceneWasLoaded(buildIndex, sceneName);
        }

        IEnumerator counterTime()
        {
            yield return new WaitForSeconds(0.01f);
            CounterManager.time = CounterManager.time + 0.01f;
            UIManager.currentText.text = CounterManager.time.ToString();
            if (CounterManager.updateTimer) MelonCoroutines.Start(counterTime()); else isCounting = false;
        }

        public void StartCounting()
        {
            isCounting = true;
            MelonCoroutines.Start(counterTime());
        }

        public override void OnUpdate()
        {
            if (enableUpdate)
            {
                if (isCounting == false && CounterManager.updateTimer)
                {
                    StartCounting();
                }
                enableUpdate = false;
            }
            base.OnUpdate();
        }
    }
}
