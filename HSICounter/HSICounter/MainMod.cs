using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MelonLoader;
using UnityEngine;
using UnityEngine.UI;

namespace HSIcounter
{
    class MainMod : MelonMod
    {
        private float LastTimeRecorded;
        private Text CounterText = null;
        private Counter CounterComponent = null;
        private bool IsInHSIScene = false;

        public override void OnSceneWasLoaded(int buildIndex, string sceneName)
        {
            MelonLogger.Msg("Scene '" + sceneName + "' has loaded.");
            if(FindCanvas() == null)
            {
                // Didn't find a canvas, create one
                CounterText = CreateText();
                CounterComponent = CounterText.gameObject.AddComponent<Counter>();
                CounterComponent.UpdateCurrentText(CounterText);
            }
            if (sceneName == "HSI")
            {
                MelonLogger.Msg("HSI Scene Loaded. Starting Timer.");
                IsInHSIScene = true;
                CounterComponent.StartCounter();
            }
            else if (sceneName == "yes")
            {
                MelonLogger.Msg("yes Scene Loaded. Ending Timer.");
                IsInHSIScene = false;
                CounterText.text = LastTimeRecorded.ToString();
            }
            CounterComponent.SetSceneName(sceneName);

            base.OnSceneWasLoaded(buildIndex, sceneName);
        }

        public override void OnLateUpdate()
        {
            if (IsInHSIScene)
            {
                LastTimeRecorded = CounterComponent.GetCurrentTime();
            }
            base.OnLateUpdate();
        }

        private GameObject FindCanvas() { return GameObject.Find("HSICounterGO"); }

        private Text CreateText()
        {
            // Create the GameObject and Add the Canvas
            GameObject myGO = new GameObject
            {
                name = "HSICounterGO"
            };
            myGO.transform.SetAsFirstSibling();
            myGO.AddComponent<Canvas>();
            // Get the Canvas Component and set its RenderMode
            Canvas myCanvas = myGO.GetComponent<Canvas>();
            myCanvas.renderMode = RenderMode.ScreenSpaceOverlay;
            // Add Canvas Stuff idk it's just in the unity docs
            myGO.AddComponent<CanvasScaler>();
            myGO.AddComponent<GraphicRaycaster>();
            // Add the Text GameObject
            GameObject myTextGO = new GameObject();
            myTextGO.transform.parent = myGO.transform;
            myTextGO.name = "HSICounterText";
            // Add the Text Component
            Text myText = myTextGO.AddComponent<Text>();
            myText.text = "Counter";
            myText.font = Resources.GetBuiltinResource(typeof(Font), "Arial.ttf") as Font;
            myText.fontSize = 50;
            myText.alignment = TextAnchor.UpperRight;
            // Text Position
            RectTransform textRectTransform = myText.GetComponent<RectTransform>();
            textRectTransform.localPosition = new Vector3(500, 200, 0);
            textRectTransform.sizeDelta = new Vector2(400, 200);
            // Return our new Text
            return myText;
        }
    }
}
