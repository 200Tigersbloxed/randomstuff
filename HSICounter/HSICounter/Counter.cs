using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace HSIcounter
{
    class Counter : MonoBehaviour
    {
        private float CurrentTime = 0.00f;
        private Text CurrentText = null;
        private bool RunTimer = false;
        private string SceneName = String.Empty;

        public float GetCurrentTime() { return CurrentTime; }
        public void SetSceneName(string sceneName) { SceneName = sceneName; }

        private void SetCounterText(string TextToApply) { CurrentText.text = TextToApply; }

        private Button YesButton = null;

        public void UpdateCurrentText(Text theText)
        {
            CurrentText = theText;
        }

        public void StartCounter()
        {
            RunTimer = true;
            StartCoroutine(CounterCoroutine());
        }

        public void StopCounter()
        {
            RunTimer = false;
            StopCoroutine(CounterCoroutine());
        }

        IEnumerator CounterCoroutine()
        {
            yield return new WaitForSeconds(0.01f);
            CurrentTime = CurrentTime + 0.01f;
            if (RunTimer)
            {
                StartCoroutine(CounterCoroutine());
            }
        }

        void Start()
        {
            AddButtonListener();
        }

        private void AddButtonListener()
        {
            if(SceneName == "HSI")
            {
                YesButton = GameObject.Find("Canvas").transform.Find("Yes").GetComponent<Button>();
                YesButton.onClick.AddListener(YesButtonClicked);
            }
        }

        void YesButtonClicked()
        {
            RunTimer = false;
        }

        void Update()
        {
            if (RunTimer)
            {
                SetCounterText(CurrentTime.ToString());
            }
        }
    }
}
